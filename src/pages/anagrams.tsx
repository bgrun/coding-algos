import styled from "styled-components";

const Container = styled.div`
  padding: 30px;
`;

const Text = styled.p`
  font-size: 20px;
  font-family: helvetica;
`;

const Anagrams = () => {
  const data = ["gab", "bag", "log", "cat", "tac", "pippen", "nippep"];

  const checkAnagrams = (data: String[]) => {
    const result: any = [];

    data.forEach((firstWord, i) => {
      const firstWordSorted = firstWord.split("").sort().join("");
      const grouping = [firstWord];

      data.forEach((secondWord, j) => {
        // No need to check the current word
        if (i !== j) {
          // Arrange the 2nd word in the same sequence as the 1st word
          const secondWordSorted = secondWord.split("").sort().join("");

          // If they two sequences are they same, they are anagrams
          if (secondWordSorted === firstWordSorted) {
            grouping.push(secondWord);
          }
        }
      });

      result.push(grouping);
    });
    return result;
  };

  const result = checkAnagrams(data);
  const listOfAnagrams = JSON.stringify(result, null, "\t");

  console.log(">", listOfAnagrams);

  return (
    <Container>
      <Text>Result: {listOfAnagrams}</Text>
    </Container>
  );
};

export default Anagrams;
