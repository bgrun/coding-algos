import styled from "styled-components";

const Container = styled.div`
  padding: 30px;
`;

const Text = styled.p`
  font-size: 20px;
  font-family: helvetica;
`;

// String s can repeat infinitely. Find how many a's are in the string
// up until the index given. E.g. s = "ada", n = 1000.
const findInString = (string, index, searchChar) => {
  let result = 0;
  let length = string.length;

  for (let i = 0; i < index; i++) {
    const currentChar = string[i % length];
    if (currentChar === searchChar) {
      result++;
    }
  }

  return result;
};

const FindInString = () => {
  const string = "ben";
  const result = findInString(string, 1, "b");

  return (
    <Container>
      <Text>Result: {result}</Text>
    </Container>
  );
};

export default FindInString;
