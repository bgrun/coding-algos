import { useEffect, useRef, useState } from "react";
import styled from "styled-components";

const Container = styled.div`
  padding: 30px;
`;

const Text = styled.p`
  font-size: 20px;
  font-family: helvetica;
`;

const List = styled.ul``;
const ListItem = styled.li`
  line-height: 40px;
`;

const ListData = styled.div<{ $expanded: boolean }>`
  display: ${(p) => (p?.$expanded ? "block" : "none")};
`;
const ExpandBtn = styled.button``;
const GenerateBtn = styled.button``;

type ActivityType = {
  activity: string;
  accessibility: number;
  key: string;
  link: string;
  participants: number;
  price: number;
  type: string;
};

const Page = () => {
  const [state, setState] = useState<ActivityType[]>([]);
  const [expandedState, setExpandedState] = useState({});
  const [disabled, setDisabled] = useState(false);
  const url = "https://www.boredapi.com/api/activity";

  const fetchData = async (url: string) => {
    const res = await fetch(url);
    const data = await res.json();
    setExpandedState(Object.assign({}, expandedState, { [data.key]: false }));
    setState([...state, data]);
  };

  useEffect(() => {
    fetchData(url);
  }, []);

  const tree = state.map((s) => (
    <ListItem key={s.key}>
      {`${s.activity}`}{" "}
      <ExpandBtn
        onClick={() => {
          setExpandedState(
            Object.assign({}, expandedState, { [s.key]: !expandedState[s.key] })
          );
        }}
      >
        Expand
      </ExpandBtn>
      <ListData $expanded={expandedState[s.key]}>
        {`${s.accessibility} ${s.key} ${s.price}`}
      </ListData>
    </ListItem>
  ));

  console.log("state", state);

  return (
    <Container>
      <GenerateBtn
        disabled={disabled}
        onClick={() => {
          fetchData(url);
          setDisabled(true);
          setTimeout(() => {
            setDisabled(false);
          }, 500);
        }}
      >
        Generate
      </GenerateBtn>
      <List>{tree}</List>
    </Container>
  );
};

export default Page;
