import styled from "styled-components";

const Container = styled.div`
  padding: 30px;
`;

const Text = styled.p`
  font-size: 20px;
  font-family: helvetica;
`;

// Return a list of numbers that pass through the filters
const filter = (data) => {
  const filters = [
    [1, 10],
    [3, 7],
    [4, 9],
  ];

  const filteredNums = data.reduce((accum, number, i) => {
    let passesFilters = true;

    filters.forEach((filter, i) => {
      if (number < filter[0] || number > filter[1]) {
        passesFilters = false;
      }
    });

    console.log("accum", i, accum, passesFilters);

    if (passesFilters) {
      return [...accum, number];
    }

    return accum;
  }, []);

  return filteredNums;
};

const FilteredNumbers = () => {
  const data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  const nums = filter(data);
  const result = JSON.stringify(nums);

  return (
    <Container>
      <Text>Result: {result}</Text>
    </Container>
  );
};

export default FilteredNumbers;
