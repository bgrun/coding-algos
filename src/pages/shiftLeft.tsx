import styled from "styled-components";

const Container = styled.div`
  padding: 30px;
`;

const Text = styled.p`
  font-size: 20px;
  font-family: helvetica;
`;

const LEFT = "left";
const RIGHT = "right";

// Given an array and a direction, shift the array in the direction
// provided by the offset
const shiftArray = (data, direction, offset) => {
  const length = data.length;

  const result: number[] = [];

  data.forEach((item, i) => {
    const newIndex = (i + offset) % length;
    result[newIndex] = item;
  });
};

const FindInString = () => {
  const data = [1, 2, 3, 4];

  const result = shiftArray(data, LEFT, 1);

  console.log(">");

  return (
    <Container>
      <Text>Result: {JSON.stringify(result)}</Text>
    </Container>
  );
};

export default FindInString;
