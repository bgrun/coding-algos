import styled from "styled-components";

const Container = styled.div`
  padding: 30px;
`;

const Text = styled.div`
  font-size: 20px;
  font-family: helvetica;
  line-height: 40px;

  > * {
    padding-left: 10px;
  }
`;

const data = {
  taxi: "a car licensed to transport passengers in return for payment of a fare",
  food: {
    sushi:
      "a traditional Japanese dish of prepared rice accompanied by seafood and vegetables",
    apple: {
      Honeycrisp:
        "an apple cultivar developed at the MAES Horticultural Research Center",
      Fuji: "an apple cultivar developed by growers at Tohoku Research Station",
    },
  },
};

const checkIfObj = (variable) =>
  typeof variable === "object" && !Array.isArray(variable) && variable !== null;

const createElTreeFromObj = (content) =>
  Object.keys(content).map((key) => {
    const isObj = checkIfObj(content[key]);
    if (isObj) {
      return (
        <Text key={key + Math.random().toString(3)}>
          <b>{key}</b>: {createElTreeFromObj(content[key])}
        </Text>
      );
    }
    return (
      <Text key={key + Math.random().toString(3)}>
        <b>{key}</b>: {content[key]}
      </Text>
    );
  });

const tree = createElTreeFromObj(data);

const FindInString = () => {
  return <Container>{tree}</Container>;
};

export default FindInString;
